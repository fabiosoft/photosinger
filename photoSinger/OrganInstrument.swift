//
//  OrganInstrument.swift
//  PicAndSound
//
//  Created by Alfonso De Crescenzo on 24/05/17.
//  Copyright © 2017 Mariarosaria Barbaraci. All rights reserved.
//

import Foundation
import AVFoundation

class OrganInstrument : MusicalInstrument {
    let namePath = "Organ_C4"
    
    override public func getPathname() -> String{
        return namePath
    }
}

