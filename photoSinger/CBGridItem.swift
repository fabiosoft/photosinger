//
//  CBGridItem.swift
//  photoSinger
//
//  Created by Fabio Nisci on 21/07/17.
//  Copyright © 2017 Fabio Nisci. All rights reserved.
//

import UIKit

enum CBItemDirection {
    case up
    case down
    case left
    case right
}

class CBGridItem: NSObject {
    var initialPosition: CGPoint?
    var position: CGPoint?  //absolute position in grid coordinate system
    var absoluteRect:CGRect? //absolute rect in image coordinate system
    var dominantColor: UIColor
    var direction: CBItemDirection
    var isCheckPoint:Bool = false
    var note: CBNote? {
        didSet{
            if let note = note{
                self.direction = note.direction
            }
        }
    }
    
    init(dominantColor: UIColor, position: CGPoint){
        self.dominantColor = dominantColor
        self.isCheckPoint = false
        self.position = position
        self.direction = .up
        self.initialPosition = position //save
    }
    
    override var description: String {
        return "CBItem \(position!):\(direction)"
    }
}
