//
//  UIImage+Crop.swift
//  photoSinger
//
//  Created by Fabio Nisci on 21/07/17.
//  Copyright © 2017 Fabio Nisci. All rights reserved.
//

import UIKit

extension UIImage {
    func crop( rect: CGRect) -> UIImage? {
        var rect = rect
        rect.origin.x*=self.scale
        rect.origin.y*=self.scale
        rect.size.width*=self.scale
        rect.size.height*=self.scale
        
        guard let cgImage = cgImage else { return nil }
        guard let imageRef = cgImage.cropping(to: rect) else { return nil }
        
        let image = UIImage(cgImage: imageRef, scale: self.scale, orientation: self.imageOrientation)
        return image
    }
}
