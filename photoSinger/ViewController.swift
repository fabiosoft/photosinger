//
//  ViewController.swift
//  photoSinger
//
//  Created by Fabio Nisci on 20/07/17.
//  Copyright © 2017 Fabio Nisci. All rights reserved.
//

import UIKit
import UIImageColors

class ViewController: UIViewController {

    @IBOutlet weak var fullImageView: UIImageView!
    @IBOutlet weak var croppedImageView: UIImageView!
    
    @IBOutlet weak var background: UIView!
    @IBOutlet weak var primary: UIView!
    @IBOutlet weak var secondary: UIView!
    @IBOutlet weak var detail: UIView!
    
    fileprivate var photoSinger:CBPhotoSinger!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        photoSinger = CBPhotoSinger()
       
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        log.error("didReceiveMemoryWarning")
    }

    @IBAction func playButtonPushed(_ sender: UIButton) {
        guard let fullImage = fullImageView.image else { return }
        let colorClassifier = CBColorClassifier(image: fullImage) //get image colors
        
        photoSinger.colors = colorClassifier
        photoSinger.fullImage = fullImage
        photoSinger.start()
        
        showDominantColors(colorClassifier: colorClassifier)
    }
    
    @IBAction func stopButtonPushed(_ sender: UIButton) {
        photoSinger.stop()
    }
    
    @IBAction func bpmSliderChanges(_ sender: UISlider) {
        log.verbose(sender.value)
        
        let newBpm = Int(sender.value)
        photoSinger.bpm = newBpm
    }
    
    
    @IBAction func organSelected(_ sender: UIButton) {
        photoSinger.instrument = OrganInstrument()
    }
    
    @IBAction func guitarSelected(_ sender: UIButton) {
        photoSinger.instrument = NylonGuitarInstrument()
    }
    
    
    fileprivate func showDominantColors(colorClassifier: CBColorClassifier){
        background.backgroundColor = colorClassifier.dominant
        primary.backgroundColor = colorClassifier.primary
        secondary.backgroundColor = colorClassifier.secondary
        detail.backgroundColor = colorClassifier.detail
    }

}

