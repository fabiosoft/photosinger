//
//  CBPhotoSinger.swift
//  photoSinger
//
//  Created by Fabio Nisci on 21/07/17.
//  Copyright © 2017 Fabio Nisci. All rights reserved.
//

import UIKit
import Colours

class CBPhotoSinger: NSObject {
    
    //QUEUES
    fileprivate let checkpointsQueue = OperationQueue()
//    fileprivate let baseQueue = OperationQueue()
    var checkpoints:[CBGridItem] = []
    
    var colors:CBColorClassifier?
    var fullImage:UIImage?
    var bpm: Int? {
        didSet{
            changeBPM(newBpm: bpm)
        }
    }
    var instrument:MusicalInstrument!{
        didSet{
            changeInstrument(newInstrument: instrument)
        }
    }
    
    //MARK: - START/STOP
    
    init(colors:CBColorClassifier? = nil) {
        super.init()
        instrument = MusicalInstrument()
        self.colors = colors
        checkpointsQueue.qualityOfService = .userInteractive
        //baseQueue.qualityOfService = .userInteractive
    }
    
    func start(){
        //startBase()
        startAssolo(completion: { (completed) in }) { (progress) in }
    }
    
    func stop(){
        //stopBase()
        stopCheckpoints()
    }

    
    
    //MARK: - LIVE CHANGES
    fileprivate func changeBPM(newBpm:Int?){
        guard let newBpm = newBpm else { return }
        for checkpointOperation:CheckpointOperation in checkpointsQueue.operations as! [CheckpointOperation]{
            checkpointOperation.bpm = newBpm
        }
    }
    
    fileprivate func changeInstrument(newInstrument:MusicalInstrument){
        for checkpointOperation:CheckpointOperation in checkpointsQueue.operations as! [CheckpointOperation]{
            checkpointOperation.instrument = newInstrument
        }
    }
    
    
    fileprivate func stopCheckpoints(){
        for o in checkpointsQueue.operations{
            o.cancel()
        }
    }
    
    private func startAssolo(completion: @escaping (_ completed:Bool) -> Void,  completionProgress: @escaping (_ progress:Progress) -> Void){
        guard let image = fullImage else {print("no image selected"); return}
        
        let gridSize:CGFloat = 7.0
        if let possibleChekpointsItems = getGridItems(from: image, size: gridSize){
            moveCheckpoints(checkpoints: possibleChekpointsItems)
        }
    }
    
    fileprivate func moveCheckpoints(checkpoints:[CBGridItem]){
        log.info("START MOVING")
        log.info("checkpoints: \(checkpoints.count)")
        
        for checkPoint in checkpoints {
            let checkpointOperation = CheckpointOperation(checkpoint: checkPoint, instrument: instrument)
            //checkpointOperation.bpm = self.bpmSolista
            checkpointsQueue.addOperation(checkpointOperation)
        }
    }
    
    fileprivate func getGridItems(from image:UIImage, size:CGFloat) -> [CBGridItem]?{
        guard let colorClassifier = colors else { return nil}
        //let numberOfCells = Int(size * size)
        let widthOfCell = image.size.width / size
        let heightOfCells = image.size.height / size
        var possibleChekpointsItems:[CBGridItem] = []
        
        for col in 0..<Int(size){
            for row in 0..<Int(size){
                //log.verbose("cropping \(col):\(row)")
                let croppingRect = CGRect(x: widthOfCell * CGFloat(row), y: heightOfCells * CGFloat(col) , width: widthOfCell, height: heightOfCells)
                //log.verbose("croppingRect: \(croppingRect)")
                if let croppedImage = image.crop(rect: croppingRect){
                    let croppedImageColors = CBColorClassifier(image: croppedImage)
                    let dominantCroppedImageColor = croppedImageColors.dominant
                    let note = CBPhotoSinger.noteFromColor(dominantColor: dominantCroppedImageColor)
                    
                    let item = CBGridItem(dominantColor: dominantCroppedImageColor, position: CGPoint(x: col, y: row))
                    item.note = note
                    item.absoluteRect = croppingRect
                    
                    
                    let distanceFromDetailColor = item.dominantColor.distance(fromColor: colorClassifier.detail)
                    if distanceFromDetailColor >= UIColor.minimumDistanceForSameColor && distanceFromDetailColor <= UIColor.maximumDistanceForSameColor{
                        //log.verbose("Distance: \(distanceFromDetailColor)")
                        //log.verbose("item: \(item.position!) could be a checkpoint")
                        item.isCheckPoint = true
                        //if possibleChekpointsItems.count < 7 {
                            possibleChekpointsItems.append(item)
                        //}
                    }
                }
            }
        }
        return possibleChekpointsItems
    }
    
    /**
     Find the nearest musical note from a color
     Example:
     with gray color -> CBNoteE
     **/
    static func noteFromColor(dominantColor:UIColor) -> CBNote?{
        let notes = CBNoteManager.allMusicalNotes()
        var knownColors:[UIColor] = []
        for note in notes{
            if let noteColor = note.color{
                knownColors.append(noteColor)
            }
        }
        
        var lastDistance:CGFloat =  dominantColor.distance(fromColor: knownColors[0])
        if var nearestColor = knownColors.first{
            for knownColor in knownColors{
                let distance = dominantColor.distance(fromColor: knownColor)
                if distance < lastDistance{
                    lastDistance = dominantColor.distance(fromColor: knownColor)
                    nearestColor = knownColor
                }
            }
            if let indexNearestColor = knownColors.index(of: nearestColor){
                return notes[indexNearestColor]
            }
        }
        return nil
    }
    
    
    
    
    
}
