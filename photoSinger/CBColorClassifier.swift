//
//  CBColorClassifier.swift
//  PicAndSound
//
//  Created by Fabio Nisci on 17/05/17.
//  Copyright © 2017 Mariarosaria Barbaraci. All rights reserved.
//

import UIKit
import UIImageColors

class CBColorClassifier {
    var dominant:UIColor
    var primary:UIColor
    var secondary:UIColor
    var detail:UIColor
 
    init(dominant:UIColor, primary:UIColor, secondary:UIColor, detail:UIColor) {
        self.dominant = dominant
        self.primary = primary
        self.secondary = secondary
        self.detail = detail
    }
    
    convenience init(imageColors: UIImageColors) {
        self.init(dominant: imageColors.backgroundColor, primary: imageColors.primaryColor, secondary: imageColors.secondaryColor, detail: imageColors.detailColor)
    }
    
    convenience init(image:UIImage) {
        let scaleDownSize = CGSize(width: 10, height: 10)
        let imageColors = image.getColors(scaleDownSize: scaleDownSize)
        self.init(dominant: imageColors.backgroundColor, primary: imageColors.primaryColor, secondary: imageColors.secondaryColor, detail: imageColors.detailColor)
    }
}
