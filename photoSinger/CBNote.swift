//
//  CBNote.swift
//  PicAndSound
//
//  Created by Fabio Nisci on 13/05/17.
//  Copyright © 2017 Mariarosaria Barbaraci. All rights reserved.
//

import UIKit

enum CBOctave {
    case twoBeforeMiddle
    case oneBeforeMiddle
    case middle
    case oneAfterMiddle
    case twoAfterMiddle
}

class CBNoteManager: NSObject{
    static func allMusicalNotes()->[CBNote]{
        return [CBNoteC(),CBNoteCsharp(),CBNoteD(),CBNoteDsharp(),CBNoteE(),CBNoteF(),CBNoteFsharp(),CBNoteG(),CBNoteGsharp(),CBNoteA(),CBNoteAsharp(),CBNoteB()]
    }
}

class CBNote: NSObject {
    
    var name:String?
    var color:UIColor?
    var pitch:Float = 0.0
    var modalScale:[CBNote]?
    var direction: CBItemDirection = .up
    
    
    
    func scalaModale() -> [CBNote]
    {
        
        
        let musicalNotes = ["C","C#","D","D#","E","F","F#","G","G#","A","A#","B"]
        let musicalNotesCBNotes = [CBNoteC(),CBNoteCsharp(),CBNoteD(),CBNoteDsharp(),CBNoteE(),CBNoteF(),CBNoteFsharp(),CBNoteG(),CBNoteGsharp(),CBNoteA(),CBNoteAsharp(),CBNoteB()]
        
        var arrayNotes:[CBNote] = []
        let posizioneIndice = musicalNotes.index(of: self.name!)!
        
        arrayNotes.insert(self, at: 0)
        
        for i in 1...6
        {
            if(i >= 3)
            {
                arrayNotes.insert(musicalNotesCBNotes[(posizioneIndice+(i*2-1))%12], at: i)
            }
            else
            {
                arrayNotes.insert(musicalNotesCBNotes[(posizioneIndice+(i*2))%12], at: i)
            }
            
        }
 
        
        return arrayNotes
 
    }
}

 
class CBNoteA: CBNote {
    override init() {
        super.init()
        name = "A"
        color = UIColor(red: 0.200, green: 0.800, blue: 0.200, alpha: 1.00)
        modalScale = []
        pitch = 900.0
        direction = .up
    }
}

class CBNoteAsharp: CBNote {
    override init() {
        super.init()
        name = "A#"
        color = UIColor(red: 0.663, green: 0.404, blue: 0.486, alpha: 1.00)
        modalScale = []
        pitch = 1000.0
        direction = .right
    }
}


class CBNoteB: CBNote {
    override init() {
        super.init()
        name = "B"
        color = UIColor(red: 0.557, green: 0.788, blue: 1.000, alpha: 1.00)
        modalScale = []
        pitch = 1100.0
        direction = .left
    }
}

class CBNoteC: CBNote {
    override init() {
        super.init()
        name = "C"
        color = UIColor(red: 1.000, green: 0.000, blue: 0.000, alpha: 1.00)
        pitch = 0.0
        direction = .down
    }
}

class CBNoteCsharp: CBNote {
    override init() {
        super.init()
        name = "C#"
        color = UIColor(red: 0.565, green: 0.000, blue: 1.000, alpha: 1.00)
        pitch = 100.0
        direction = .down
    }
}


class CBNoteD: CBNote {
    override init() {
        super.init()
        name = "D"
        color = UIColor(red: 1.000, green: 1.000, blue: 0.000, alpha: 1.00)
        modalScale = []
        pitch = 200.0
        direction = .right
    }
}

class CBNoteDsharp: CBNote {
    override init() {
        super.init()
        name = "D#"
        color = UIColor(red: 0.718, green: 0.275, blue: 0.545, alpha: 1.00)
        pitch = 300.0
        direction = .down
    }
}

class CBNoteE: CBNote {
    override init() {
        super.init()
        name = "E"
        color = UIColor(red: 0.765, green: 0.949, blue: 1.000, alpha: 1.00)
        modalScale = []
        pitch = 400.0
        direction = .up
    }
}


class CBNoteF: CBNote {
    override init() {
        super.init()
        name = "F"
        color = UIColor(red: 0.671, green: 0.000, blue: 0.204, alpha: 1.00)
        modalScale = []
        pitch = 500.0
        direction = .right
    }
}

class CBNoteFsharp: CBNote {
    override init() {
        super.init()
        name = "F#"
        color = UIColor(red: 0.498, green: 0.545, blue: 0.992, alpha: 1.00)
        modalScale = []
        pitch = 600.0
        direction = .left
    }
}

class CBNoteG: CBNote {
    override init() {
        super.init()
        name = "G"
        color = UIColor(red: 1.000, green: 0.498, blue: 0.000, alpha: 1.00)
        modalScale = []
        pitch = 700.0
        direction = .right
    }
}

class CBNoteGsharp: CBNote {
        override init() {
            super.init()
            name = "G#"
            color = UIColor(red: 0.733, green: 0.459, blue: 0.988, alpha: 1.00)
            modalScale = []
            pitch = 800.0
            direction = .down
    }
}




