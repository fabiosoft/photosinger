//
//  CheckpointOperation.swift
//  PicAndSound
//
//  Created by Fabio Nisci on 22/05/17.
//  Copyright © 2017 Mariarosaria Barbaraci. All rights reserved.
//

import UIKit
import AVFoundation
import SwiftySound

class CheckpointOperation: Operation {
    
    fileprivate var checkpoint:CBGridItem!
    var instrument:MusicalInstrument!
    
    var bpm: Int?
    
    
    override var isAsynchronous: Bool { return true }
    override var isExecuting: Bool { return state == .executing }
    override var isFinished: Bool { return state == .finished }
    
    init(checkpoint:CBGridItem, instrument:MusicalInstrument) {
        super.init()
        self.checkpoint = checkpoint
        self.instrument = instrument
    }
    
    var state = State.ready {
        willSet {
            willChangeValue(forKey: state.keyPath)
            willChangeValue(forKey: newValue.keyPath)
        }
        didSet {
            didChangeValue(forKey: state.keyPath)
            didChangeValue(forKey: oldValue.keyPath)
        }
    }
    
    enum State: String {
        case ready = "Ready"
        case executing = "Executing"
        case finished = "Finished"
        fileprivate var keyPath: String { return "is" + self.rawValue }
    }
    
    override func start() {
        if self.isCancelled {
            state = .finished
        } else {
            state = .ready
            main()
        }
    }
    
    var movingPoint:CBGridItem!
    var oldRow:Int = 0
    var oldCol:Int = 0
    
    override func main() {

        movingPoint = checkpoint!
        oldRow = Int(movingPoint.position!.y)
        oldCol = Int(movingPoint.position!.x)
        //Thread.sleep(forTimeInterval: sleep)

        while(self.isCancelled == false){
            state = .executing
            move()
        }
        state = .finished
         
    }
    
    fileprivate func move(){
        let end:Int = 6
        
        var newRow:Int = 0
        var newCol:Int = 0
        let sleep = TimeInterval(durataNote())
        
        if(checkpoint.direction == .down){
            for row in stride(from: Int(movingPoint.position!.x), through: end, by: 1){
                newRow = row
                newCol = oldCol
                movingPoint.position = CGPoint(x: newRow, y: newCol)
                oldRow = newRow
                oldCol = newCol
                log.verbose("\(movingPoint.position!)|\(movingPoint.direction)")
                log.verbose("waiting for \(sleep) sec")
                Thread.sleep(forTimeInterval: sleep)
            }
            checkpoint.direction = .up
            log.verbose("play DOWN")
        
        }else if(checkpoint.direction == .up){
            for row in stride(from: Int(movingPoint.position!.x), through: 0, by: -1){
                newRow = row
                newCol = oldCol
                movingPoint.position = CGPoint(x: newRow, y: newCol)
                oldRow = newRow
                oldCol = newCol
                log.verbose("\(movingPoint.position!)|\(movingPoint.direction)")
                log.verbose("waiting for \(sleep) sec")
                Thread.sleep(forTimeInterval: sleep)
            }
            checkpoint.direction = .down
            log.verbose("play UP")
        
        }else if(checkpoint.direction == .left){
            for col in stride(from: Int(movingPoint.position!.y), through: 0, by: -1){
                newRow = oldRow
                newCol = col
                movingPoint.position = CGPoint(x: newRow, y: newCol)
                oldRow = newRow
                oldCol = newCol
                log.verbose("\(movingPoint.position!)|\(movingPoint.direction)")
                log.verbose("waiting for \(sleep) sec")
                Thread.sleep(forTimeInterval: sleep)
            }
            checkpoint.direction = .right
            log.verbose("play LEFT")
        }else if(checkpoint.direction == .right){
            for col in stride(from: Int(movingPoint.position!.y) + 1, through: end, by: 1){
                newRow = oldRow
                newCol = col
                movingPoint.position = CGPoint(x: newRow, y: newCol)
                oldRow = newRow
                oldCol = newCol
                log.verbose("\(movingPoint.position!)|\(movingPoint.direction)")
                log.verbose("waiting for \(sleep) sec")
                Thread.sleep(forTimeInterval: sleep)
            }
            checkpoint.direction = .left
            log.verbose("play RIGHT")
        }
        
        //log.verbose("suona: \(movingPoint)")
        movingPoint.position = CGPoint(x: oldRow, y: oldCol)
        //engine.playSound(audioFile: audioFile, pitch: pitch)
        
        
        // File format: instrument_note_pitch.mp3
        let instrumentName = instrument.getPathname().replacingOccurrences(of: "_C4", with: "")
        let noteName = checkpoint.note!.name!
        let pitch = Int(checkpoint.note!.pitch)
        let filename = "\(instrumentName)_\(noteName)_\(pitch)"
        if !self.isCancelled{
            Sound.play(file: filename, fileExtension: "mp3", numberOfLoops: 1)
            //NotificationCenter.default.post(name: CBNotification.CBNoteDidPlayNotification, object: self, userInfo: ["filename": filename, "movingpoint": movingPoint])
        }
    }
    
    
    func durataNote() -> Float{
        guard let bpm = bpm else { return 1.0 }
        return Float(((60)/Double(bpm)))
    }
}


