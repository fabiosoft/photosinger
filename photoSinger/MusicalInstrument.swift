//
//  MusicalInstrument.swift
//  photoSinger
//
//  Created by Fabio Nisci on 21/07/17.
//  Copyright © 2017 Fabio Nisci. All rights reserved.
//

import Foundation

class MusicalInstrument {
    
    func getPathname() -> String {
        return "AcousticPiano_C4"
    }
}
