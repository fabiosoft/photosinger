//
//  UIColor+Random.swift
//  photoSinger
//
//  Created by Fabio Nisci on 21/07/17.
//  Copyright © 2017 Fabio Nisci. All rights reserved.
//

import UIKit

extension UIColor{
    static let minimumDistanceForSameColor:CGFloat = 30.0 // more than this probably the color is the same
    static let maximumDistanceForSameColor:CGFloat = 60.0 // less than this probably the color is the same
    
    static func getRandomColor() -> UIColor{
        let randomRed:CGFloat = CGFloat(drand48())
        let randomGreen:CGFloat = CGFloat(drand48())
        let randomBlue:CGFloat = CGFloat(drand48())
        return UIColor(red: randomRed, green: randomGreen, blue: randomBlue, alpha: 1.0)
    }
}
