//
//  NylonGuitarInstrument.swift
//  pitcher
//
//  Created by Alfonso De Crescenzo on 12/05/17.
//  Copyright © 2017 Fabio Nisci. All rights reserved.
//


import Foundation
import AVFoundation

class NylonGuitarInstrument : MusicalInstrument {
    let namePath = "NylonGuitar_C4"
 
    override public func getPathname() -> String {
        return namePath
    }
}
